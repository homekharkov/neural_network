import numpy as np
x=np.array([[-2, -1],    #обучающий массив, но на вход при вызове сети подается вектор х=[x1,x2]
            [25, 6],
            [17, 4],
            [-15, -6]])
y =np.array([[1,0,0,1]])
def F(x):
    return 1/(1+np.exp(-x))
def proizvod(x):
    return F(x)*(1-F(x))
def func_scalar_proizved(x,w):
    s_pr =0
    for i in range(0, len(x)):
        for j in range(0, len(w)):
            s_pr = x[i]*w[j]
    return s_pr
w = 0.5*np.random.rand(2, 4)   # матрица весов 2 на 4
b = 1  # вектор смещения
for i in range(0,40000):
    for matr, y_true in zip(x, y):
        t=np.dot(matr,w)+b
        #print(t)
        h=F(t)
        #print("то что предполагает сеть")
        #print(h)
        err=h-y_true
        #print("Oshibka")
        #print(err)
        d_E_d_h=err*proizvod(h)
        #print("d_E_d_h")
        #print(d_E_d_h)
        d_E_d_t=d_E_d_h*proizvod(t)
        #print("d_E_d_t")
        #print(d_E_d_t)
        d_E_d_w =np.array([matr]).T*d_E_d_t
        #print("d_E_d_w")
        #print(d_E_d_w)
        w-=0.1*d_E_d_w          #w=w+err*proizvod(h)*matr[0]*0.1
        b=d_E_d_t

def train(array):
    o1=F(func_scalar_proizved(h,w[0])+b)
    o2=F(func_scalar_proizved(h,w[1])+b)
    o=F(func_scalar_proizved(o1,w[0])*array[0][0]+func_scalar_proizved(o2,w[1])*array[0][1])
    print('Единица - девочка, ноль - мальчик')
    print(o)

emily = np.array([[-7, -3]])  # 128 фунтов, 63 дюйма
frank = np.array([[25, 2]])  # 155 фунтов, 68 дюймов
h1=train(emily)
h2=train(frank)
# print(h1)  # 0.951 - F
#print("Frank: %.3f" % network.feedforward(frank1))  # 0.039 - M

